package com.acooly.module.openapi.client.provider.hx.message.xStream.withdraw.request;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.io.naming.NoNameCoder;
import com.thoughtworks.xstream.io.xml.DomDriver;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

/**
 * @author fufeng 2018/2/28 19:43.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("Detail")
public class ReqWithdrawDetail {

    /**
     *商户自定义，只能数字或者大小写英文字符，
     *保证唯一性
     */
    @XStreamAlias("MerBillNo")
    private String merBillNo;
    /**
     *委托付款银行卡的账户名
     *这里的长度是指最大支持 25 个汉字
     */
    @XStreamAlias("AccountName")
    private String accountName;
    /**
     *企业银行账户号，或者个人银行卡号
     */
    @XStreamAlias("AccountNumber")
    private String accountNumber;
    /**
     *银行卡所属银行的全称
     *这里的长度是指最大支持 25 个汉字
     */
    @XStreamAlias("BankName")
    private String bankName;
    /**
     *支行名称
     *这里的长度是指最大支持 25 个汉字
     *BizId=1 且 ChannelId=3 时必填，
     *其它方式非必填
     */
    @XStreamAlias("BranchBankName")
    private String branchBankName;
    /**
     *例如：苏州市
     *这里的长度是指最大支持 25 个汉字
     *BizId=1 且 ChannelId=3 时必填，
     *其它方式非必填
     */
    @XStreamAlias("BankCity")
    private String bankCity;
    /**
     *例如：江苏省
     *这里的长度是指最大支持 25 个汉字
     *BizId=1 且 ChannelId=3 时必填，
     *其它方式非必填
     */
    @XStreamAlias("BankProvince")
    private String bankProvince;
    /**
     * 10.00
     */
    @XStreamAlias("BillAmount")
    private String billAmount;
    /**
     *身份证号
     *BizId=1 且 ChannelId=3 时必填，
     *其它方式非必填
     */
    @XStreamAlias("IdCard")
    private String idCard;
    /**
     *BizId=1 且 ChannelId=3 时必填，
     *其它方式非必填
     */
    @XStreamAlias("MobilePhone")
    private String mobilePhone;

    @XStreamOmitField
    private XStream xStream;

    /**
     * 数据类型 xml/json
     *
     * @param
     */
    public ReqWithdrawDetail() {
        xStream = new XStream(new DomDriver("UTF-8", new NoNameCoder()));
        // 启用Annotation
        xStream.autodetectAnnotations(true);
    }

    public String obj2Str(Object obj) {
        return xStream.toXML(obj);
    }

}
