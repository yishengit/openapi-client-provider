/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-11-15 13:42 创建
 */
package com.acooly.module.openapi.client.provider.newyl.partner.impl;


import com.acooly.module.openapi.client.provider.newyl.NewYlProperties;
import com.acooly.module.openapi.client.provider.newyl.domain.NewYlResponse;
import com.acooly.module.openapi.client.provider.newyl.partner.NewYlPartnerIdLoadManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 此为获取partnerId的空实现 （demo）
 *
 * @author zhangpu 2017-11-15 13:42
 */
@Slf4j
@Service("newYlPartnerIdLoadManager")
public class NewYlEmptyPartnerIdLoader implements NewYlPartnerIdLoadManager {

    @Autowired
    private NewYlProperties newYlProperties;

    @Override
    public String load(NewYlResponse ylResponse) {
        return ylResponse.getService();
    }
}
