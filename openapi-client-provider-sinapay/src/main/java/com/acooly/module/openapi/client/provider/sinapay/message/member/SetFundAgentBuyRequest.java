/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月29日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.SMT_FUND_AGENT_BUY, type = ApiMessageType.Request)
public class SetFundAgentBuyRequest extends SinapayRequest {

	/** 用户标识信息 */
	@Size(max = 50)
	@NotEmpty
	@ApiItem("identity_id")
	private String identityId;

	/** 用户标识类型 */
	@Size(max = 16)
	@NotEmpty
	@ApiItem("identity_type")
	private String identityType = "UID";

	/** 经办人姓名 */
	@Size(max = 50)
	@NotEmpty
	@ApiItem(value = "agent_name", securet = true)
	private String agentName;

	/** 经办人身份证 */
	@Size(max = 16)
	@NotEmpty
	@ApiItem("license_type_code")
	private String licenseTypeCode = "ID";

	/** 经办人身份证 */
	@Size(max = 18)
	@NotEmpty
	@ApiItem(value = "license_no", securet = true)
	private String licenseNo;

	/** 经办人手机号 */
	@Size(max = 32)
	@NotEmpty
	@ApiItem(value = "agent_mobile", securet = true)
	private String agentMobile;

	/** 经办人邮箱 */
	@Size(max = 32)
	@ApiItem("email")
	private String email;
	
	/**
	 * 用户IP/运营商IP
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "client_ip")
	private String clientIp;
}
