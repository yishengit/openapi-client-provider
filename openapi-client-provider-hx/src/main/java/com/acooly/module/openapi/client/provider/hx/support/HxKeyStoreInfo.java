package com.acooly.module.openapi.client.provider.hx.support;

import com.acooly.module.safety.support.KeySupport;

import lombok.extern.slf4j.Slf4j;

/**
 * @author zhike 2018/2/6 13:47
 * .key文件加载
 */
@Slf4j
public class HxKeyStoreInfo extends KeySupport {

    /**
     * 向量
     */
    private String deskey;

    /**
     * 密钥
     */
    private String desiv;

    /**
     * 解密MD5串
     */
    private String MD5Key;

    public String getDeskey() {
        return deskey;
    }

    public void setDeskey(String deskey) {
        this.deskey = deskey;
    }

    public String getDesiv() {
        return desiv;
    }

    public void setDesiv(String desiv) {
        this.desiv = desiv;
    }

    public String getMD5Key() {
        return MD5Key;
    }

    public void setMD5Key(String MD5Key) {
        this.MD5Key = MD5Key;
    }
}
