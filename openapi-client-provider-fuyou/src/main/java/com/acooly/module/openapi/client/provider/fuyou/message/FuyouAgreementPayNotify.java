package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouNotify;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.acooly.module.openapi.client.provider.fuyou.support.FuyouAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.AGREEMENT_PAY,type = ApiMessageType.Notify)
@XStreamAlias("RESPONSE")
public class FuyouAgreementPayNotify extends FuyouNotify{

    /**
     * 交易类型
     */
    @FuyouAlias("TYPE")
    private String type;

    /**
     * 用户编号
     * 商户端用户的唯一编号，即用户 ID
     */
    @FuyouAlias("USERID")
    @NotBlank
    @Size(max = 40)
    private String userId;

    /**
     * 响应代码
     * 0000 成功
     */
    @FuyouAlias("RESPONSECODE")
    private String responseCode;

    /**
     * 响应中文描述
     */
    @FuyouAlias("RESPONSEMSG")
    private String responseMsg;

    /**
     * 商户订单号
     * 商户订单流水号商户确保唯一
     */
    @Size(max = 60)
    @NotBlank
    @FuyouAlias("MCHNTORDERID")
    private String merchOrderNo;

    /**
     * 富友订单号
     * 富友生成的订单号，该订单号在相
     当长的时间内不重复。富友通过订
     单号来唯一确认一笔订单的重复性
     */
    @FuyouAlias("ORDERID")
    private String bankOrderId;

    /**
     * 交易金额
     * 交易金额，分为单位
     */
    @FuyouAlias("AMT")
    @NotBlank
    @Size(max = 12)
    private String amount;

    /**
     * 银行卡号
     * 请求报文中的银行卡号（版本号传
     * 2.0 时按请求参数返回）
     */
    @FuyouAlias("BANKCARD")
    @NotBlank
    @Size(max = 20)
    private String bankCard;

    /**
     * 协议号
     * 交易成功好默认绑定五要素的协议
     * 号（信用卡暂不支持绑定协议）
     */
    @FuyouAlias("PROTOCOLNO")
    @Size(max = 30)
    private String protocolNo;

    /**
     * 支付接口所
     * 需字段
     * 调支付接口时原样传送该字段
     */
    @FuyouAlias("SIGNPAY")
    private String signPay;

    /**
     * 签名类型
     */
    @FuyouAlias("SIGNTP")
    private String signType;

    /**
     * 摘要数据
     */
    @FuyouAlias("SIGN")
    private String sign;

    @Override
    public String getSignStr() {
        return getType()+"|"+getVersion()+"|"+getResponseCode()+"|"+getPartner()+"|"+getMerchOrderNo()
                +"|"+getBankOrderId()+"|"+getAmount()+"|"+getBankCard()+"|";
    }
}
