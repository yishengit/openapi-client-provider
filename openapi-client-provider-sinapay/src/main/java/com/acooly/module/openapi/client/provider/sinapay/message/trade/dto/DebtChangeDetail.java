package com.acooly.module.openapi.client.provider.sinapay.message.trade.dto;

import com.acooly.module.openapi.client.provider.sinapay.annotation.ApiDto;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ItemOrder;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinaflagType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinafundType;
import com.acooly.module.openapi.client.provider.sinapay.message.Dtoable;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 债权变动明细
 *
 * @author xiaohong
 * @create 2018-07-17 11:54
 **/
@Getter
@Setter
@ApiDto
public class DebtChangeDetail implements Dtoable {
    /**
     * 投资人标识ID
     */
    @NotEmpty
    @Size(max = 32)
    @ItemOrder(0)
    private String investorId;

    /**
     * 投资人标识类型
     *
     * 参考： 标识类型
     */
    @NotEmpty
    @Size(max = 16)
    @ItemOrder(1)
    private String investorIdType = SinaflagType.UID.getCode();

    /**
     * 借款人标识ID
     */
    @NotEmpty
    @Size(max = 32)
    @ItemOrder(2)
    private String borrowerId;

    /**
     * 借款人标识类型
     *
     * 参考： 标识类型
     */
    @Size(max = 16)
    @ItemOrder(3)
    private String borrowerIdType = SinaflagType.UID.getCode();

    /**
     * 金额
     * Number(15,2)
     */
    @NotEmpty
    @ItemOrder(4)
    private String amount;

    /**
     * 资金类型
     *
     * 参考： 资金类型
     */
    @NotEmpty
    @Size(max = 32)
    @ItemOrder(5)
    private String fundType = SinafundType.PRINCIPAL.getCode();

    /**
     * 备注
     */
    @Size(max = 64)
    @ItemOrder(6)
    private String remark;
}
