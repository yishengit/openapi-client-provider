package com.acooly.openapi.client.provider.wsbank.ordershare;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.wsbank.WsbankApiService;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankOrderShareQueryRequest;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankOrderShareQueryResponse;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankOrderShareRequest;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankOrderShareResponse;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankOrderShareQueryRequestBody;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankOrderShareQueryRequestInfo;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankOrderShareRequestBody;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankOrderShareRequestInfo;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 4.5.1	分账接口
 * @author sunjx
 *
 */
@SpringBootApplication
@BootApp(sysName = "wsbankOrderShareTest")
public class WsbankOrderShareTest extends NoWebTestBase {

    @Autowired
    private WsbankApiService wsbankApiService;

    /**
     * 分账测试
     */
    @Test
    public void orderShareTest() {
        WsbankOrderShareRequestBody body = new WsbankOrderShareRequestBody();
        body.setOutTradeNo(Ids.oid());//请求流水号
        body.setMerchantId("226801000000114869124");//收款方商户号
        body.setRelateOrderNo("2018062810152010070001120000220365");//关联网商订单号
        body.setTotalAmount("2");//订单金额(金额为分)
        //付款方资金明细记录
        //List<FundDetails> payerDetails = new ArrayList<>();
        //FundDetails payer = new FundDetails();
        //payer.setAmount("1");
        //payer.setParticipantId("226801000000114223120");//商户
        ///payer.setParticipantType("MERCHANT");
        //payerDetails.add(payer);
        //body.setPayerFundDetails(JSON.toJSONString(payerDetails));
        //收款方资金明细记录
        /*List<FundDetails> payeeDetails = new ArrayList<>();
        FundDetails payee = new FundDetails();
        payee.setAmount("1");
        payee.setParticipantId("202210000000000001234");//汇通达
        payee.setParticipantType("PLATFORM");
        payeeDetails.add(payee);
        body.setPayeeFundDetails(JSON.toJSONString(payeeDetails));*/
        WsbankOrderShareRequestInfo requestInfo = new WsbankOrderShareRequestInfo();
        requestInfo.setWsbankOrderShareRequestBody(body);
        WsbankOrderShareRequest request = new WsbankOrderShareRequest();
        request.setRequestInfo(requestInfo);
        WsbankOrderShareResponse response = wsbankApiService.orderShare(request);
        System.out.println("测试订单分账接口结果为:" + JSON.toJSONString(response));
    }
    
    /**
     * 单笔分账查询
     */
    @Test
    public void ordershareQuery() {
        WsbankOrderShareQueryRequestBody body = new WsbankOrderShareQueryRequestBody();
        body.setMerchantId("226801000000114869124");//收款方商户号
        body.setOutTradeNo("o18062820583361560002");//请求流水号
        body.setRelateOrderNo("2018062810152010070001120000221725");//关联网商订单号
        body.setShareOrderNo("2018062810152060086008120000005159");//分账单号
        WsbankOrderShareQueryRequestInfo requestInfo = new WsbankOrderShareQueryRequestInfo();
        requestInfo.setBody(body);
        WsbankOrderShareQueryRequest request = new WsbankOrderShareQueryRequest();
        request.setRequestInfo(requestInfo);
        WsbankOrderShareQueryResponse response = wsbankApiService.orderShareQuery(request);
        System.out.println("测试订单分账查询结果为:" + JSON.toJSONString(response));
    }
}
