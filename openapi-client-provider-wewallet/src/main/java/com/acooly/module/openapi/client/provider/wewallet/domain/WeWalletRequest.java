/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.wewallet.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhangpu
 */
@Getter
@Setter
public class WeWalletRequest extends WeWalletApiMessage {
}
