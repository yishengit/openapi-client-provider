/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 17:43 创建
 */
package com.acooly.module.openapi.client.provider.bosc.enums.member;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author zhangpu 2017-09-25 17:43
 */
public enum BoscAuthListEnum implements Messageable {

    TENDER("TENDER", "自动投标"),

    REPAYMENT("REPAYMENT", "自动还款"),

    CREDIT_ASSIGNMENT("CREDIT_ASSIGNMENT", "自动债权认购"),

    COMPENSATORY("COMPENSATORY", "自动代偿"),

    WITHDRAW("WITHDRAW", "自动提现"),

    RECHARGE("RECHARGE", "自动充值");


    private final String code;
    private final String message;

    private BoscAuthListEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (BoscAuthListEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static BoscAuthListEnum find(String code) {
        for (BoscAuthListEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("BoscAuthListEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<BoscAuthListEnum> getAll() {
        List<BoscAuthListEnum> list = new ArrayList<BoscAuthListEnum>();
        for (BoscAuthListEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (BoscAuthListEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
