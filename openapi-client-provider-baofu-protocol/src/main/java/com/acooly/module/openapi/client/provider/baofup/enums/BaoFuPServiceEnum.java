/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.baofup.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike Date: 2017-03-30 15:12:06
 */
public enum BaoFuPServiceEnum implements Messageable {
  BILL_DOWNLOAD("billDowdload", "0", "下载对账文件"),
  PROTOCOL_PAY_APPLY_BIND_CARD("protocolPayApplyBindCard", "01", "协议支付预绑卡类交易"),
  PROTOCOL_PAY_CONFIRM_BIND_CARD("protocolPayConfirmBindCard", "02", "协议支付确认绑卡类交易"),
  PROTOCOL_PAY("protocolPay", "08", "协议支付直接支付类"),
  PROTOCOL_PAY_UNBIND_CARD("protocolPayUnbindCard", "04", "协议支付解除绑卡类交易"),
  PROTOCOL_PAY_QUERY_BIND_CARD("protocolPayQueryBindCard", "03", "查询绑定关系类交易"),
  PROTOCOL_PAY_QUERY("protocolPayQuery", "07", "协议支付订单查询类交易"),
  PROTOCOL_APPLY_PAY("protocolApplyPay", "05", "协议支付预支付类交易"),
  PROTOCOL_CONFIRM_PAY("protocolConfirmPay", "06", "协议支付确认支付类交易"),
  ;

  private final String code;
  private final String key;
  private final String message;

  private BaoFuPServiceEnum(String code, String key, String message) {
    this.code = code;
    this.message = message;
    this.key = key;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public String code() {
    return code;
  }

  public String message() {
    return message;
  }

  public String getKey() {
    return key;
  }

  public static Map<String, String> mapping() {
    Map<String, String> map = new LinkedHashMap<String, String>();
    for (BaoFuPServiceEnum type : values()) {
      map.put(type.getCode(), type.getMessage());
    }
    return map;
  }

  /**
   * 通过枚举值码查找枚举值。
   *
   * @param code 查找枚举值的枚举值码。
   * @return 枚举值码对应的枚举值。
   * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
   */
  public static BaoFuPServiceEnum find(String code) {
    for (BaoFuPServiceEnum status : values()) {
      if (status.getCode().equals(code)) {
        return status;
      }
    }
    return null;
  }

  /**
   * 通过枚举值码查找枚举值。
   *
   * @param key 查找枚举值的枚举值码。
   * @return 枚举值码对应的枚举值。
   * @throws IllegalArgumentException 如果 key 没有对应的 Status 。
   */
  public static BaoFuPServiceEnum findByKey(String key) {
    for (BaoFuPServiceEnum status : values()) {
      if (status.getKey().equals(key)) {
        return status;
      }
    }
    return null;
  }

  /**
   * 获取全部枚举值。
   *
   * @return 全部枚举值。
   */
  public static List<BaoFuPServiceEnum> getAll() {
    List<BaoFuPServiceEnum> list = new ArrayList<BaoFuPServiceEnum>();
    for (BaoFuPServiceEnum status : values()) {
      list.add(status);
    }
    return list;
  }

  /**
   * 获取全部枚举值码。
   *
   * @return 全部枚举值码。
   */
  public static List<String> getAllCode() {
    List<String> list = new ArrayList<String>();
    for (BaoFuPServiceEnum status : values()) {
      list.add(status.code());
    }
    return list;
  }
}
