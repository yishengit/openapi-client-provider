package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouResponse;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_WITHDRAW,type = ApiMessageType.Response)
@XStreamAlias("payforrsp")
public class FuyouWithdrawResponse extends FuyouResponse{


    /**
     * 响应码
     */
    @XStreamAlias("ret")
    @Size(max = 6)
    private String ret;

    /**
     * 备注
     * 填写后，系统体现在交易查询中
     */
    @XStreamAlias("memo")
    @Size(max = 1024)
    private String memo;

    /**
     * 交易状态类别
     */
    @XStreamAlias("transStatusDesc")
    @Size(max = 60)
    private String transStatusDesc;
}
