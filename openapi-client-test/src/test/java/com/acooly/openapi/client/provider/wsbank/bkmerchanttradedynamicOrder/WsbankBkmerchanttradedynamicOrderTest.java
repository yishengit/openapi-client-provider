package com.acooly.openapi.client.provider.wsbank.bkmerchanttradedynamicOrder;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.wsbank.WsbankApiService;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankBkmerchanttradedynamicOrderRequest;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankBkmerchanttradedynamicOrderResponse;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBkmerchanttradedynamicOrderRequestBody;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBkmerchanttradedynamicOrderRequestInfo;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@BootApp(sysName = "wsbankBkmerchanttradedynamicOrderTest")
public class WsbankBkmerchanttradedynamicOrderTest extends NoWebTestBase {
    @Autowired
    private WsbankApiService wsbankApiService;
    
    /**
     * 测试动态订单扫码支付接口
     */
    @Test
    public void bkmerchanttradedynamicOrderByChannelTypeALI() {
    	WsbankBkmerchanttradedynamicOrderRequestBody requestBody = new WsbankBkmerchanttradedynamicOrderRequestBody();
        requestBody.setOutTradeNo(Ids.oid());
        requestBody.setGoodsid("good001");
        requestBody.setBody("测试商品");
        requestBody.setTotalAmount("1");
        requestBody.setMerchantId("226801000000103688124");
        requestBody.setChannelType("ALI");
        requestBody.setDeviceCreateIp("172.16.1.11");
        WsbankBkmerchanttradedynamicOrderRequestInfo requestInfo = new WsbankBkmerchanttradedynamicOrderRequestInfo();
        requestInfo.setRequestBody(requestBody);
        WsbankBkmerchanttradedynamicOrderRequest request = new WsbankBkmerchanttradedynamicOrderRequest();
        request.setRequestInfo(requestInfo);
        WsbankBkmerchanttradedynamicOrderResponse response = wsbankApiService.bkmerchanttradedynamicOrder(request);
        System.out.println("动态订单扫码支付接口："+ JSON.toJSONString(response));
    }
    
    /**
     * 测试动态订单扫码支付接口
     */
    @Test
    public void bkmerchanttradedynamicOrderByChannelTypeWX() {
    	WsbankBkmerchanttradedynamicOrderRequestBody requestBody = new WsbankBkmerchanttradedynamicOrderRequestBody();
        requestBody.setOutTradeNo(Ids.oid());
        requestBody.setGoodsid("good001");
        requestBody.setBody("测试商品");
        requestBody.setTotalAmount("10000");
        requestBody.setMerchantId("226801000000103688124");
        requestBody.setChannelType("WX");
        requestBody.setDeviceCreateIp("172.16.1.11");
        WsbankBkmerchanttradedynamicOrderRequestInfo requestInfo = new WsbankBkmerchanttradedynamicOrderRequestInfo();
        requestInfo.setRequestBody(requestBody);
        WsbankBkmerchanttradedynamicOrderRequest request = new WsbankBkmerchanttradedynamicOrderRequest();
        request.setRequestInfo(requestInfo);
        WsbankBkmerchanttradedynamicOrderResponse response = wsbankApiService.bkmerchanttradedynamicOrder(request);
        System.out.println("动态订单扫码支付接口："+ JSON.toJSONString(response));
    }
}
