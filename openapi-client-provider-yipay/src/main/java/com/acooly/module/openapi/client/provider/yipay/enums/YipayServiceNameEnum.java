/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.yipay.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * BOSC服务名称枚举
 *
 * @author zhangpu 2017-09-24 19:29
 */
public enum YipayServiceNameEnum implements Messageable {

    UNKNOWN("UNKNOWN", "未知", YipayApiServiceType.SYNC),
    THREE_ELEMENTS_VERIFY("/mapi/FAS/V5/bankCardVerify/threeElementsVerify", "银行卡三要素验证", YipayApiServiceType.SYNC),
    FOUR_ELEMENTS_VERIFY("/mapi/FAS/V5/bankCardVerify/fourElementsVerify", "银行卡四要素验证", YipayApiServiceType.SYNC),
    OPERATOR_THREE_ELMS_VALID("/mapi/FAS/threeElmsValid/valid", "全网运营商三要素验证", YipayApiServiceType.SYNC),
    REALTIME_LOCATION_VERIFY_POSITION("/mapi/FAS/V5/realTimeLocation/verifyPosition", "全网实时位置验证接口", YipayApiServiceType.SYNC),
    CITY_LOCATION_VERIFY_LOCATION("/mapi/FAS/V5/cityLocationVerify/verifyLocation", "运营商实时城市位置核验接口", YipayApiServiceType.SYNC),
    PHONEINFO_VALID_ONLINE_STATUS("/mapi/FAS/V5/phoneInfoValid/onlineStatusValid", "运营商在网状态查询", YipayApiServiceType.SYNC),
    WHOLE_NETWORK_MOBILE_TIME_QUERY("/mapi/FAS/V5/wholeNetworkMobileTime/queryNetworkTime", "运营商入网时长查询", YipayApiServiceType.SYNC),
    TWO_ELEM_VALID_BYID("/mapi/FAS/V5/twoElemValid/validById", "电信运营商二要素验证(手机号+身份证)", YipayApiServiceType.SYNC),
    TWO_ELEM_VALID_BYNAME("/mapi/FAS/V5/twoElemValid/validByName", "电信运营商二要素验证(手机号+姓名)", YipayApiServiceType.SYNC),
    CERT_TWO_ELEMENTS_VERIFY("/mapi/FAS/V5/certTwoElementsVerify/verify", "身份证二要素验证接口", YipayApiServiceType.SYNC),
    DEDUCT("/mapi/FAS/agentReceive/receiveToAcct", "实时裸代扣", YipayApiServiceType.ASYNC),
    TRADE_QUERY("/mapi/FAS/agentReceiveQuery/queryOrderState", "交易查询", YipayApiServiceType.SYNC),
    ;

    private final String code;
    private final String message;
    private final YipayApiServiceType apiServiceType;

    private YipayServiceNameEnum(String code, String message, YipayApiServiceType apiServiceType) {
        this.code = code;
        this.message = message;
        this.apiServiceType = apiServiceType;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    public YipayApiServiceType getApiServiceType() {
        return apiServiceType;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (YipayServiceNameEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static YipayServiceNameEnum find(String code) {
        for (YipayServiceNameEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("BoscServiceNameEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<YipayServiceNameEnum> getAll() {
        List<YipayServiceNameEnum> list = new ArrayList<YipayServiceNameEnum>();
        for (YipayServiceNameEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (YipayServiceNameEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
