package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.core.common.exception.OrderCheckException;
import com.acooly.core.common.facade.ResultCode;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPRequest;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/13 14:29
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.PROTOCOL_PAY_QUERY_BIND_CARD,type = ApiMessageType.Request)
public class BaoFuProtocolPayQueryBindCardRequest extends BaoFuPRequest {

    /**
     * 用户ID
     * 用户在商户平台唯一ID
     */
    @Size(max = 50)
    @BaoFuPAlias(value = "user_id")
    private String userId;

    /**
     * 银行卡号
     * 与user_id必须其中一个有值
     * 加密方式：Base64转码后，使用数字信封指定的方式和密钥加密
     */
    @Size(max = 126)
    @BaoFuPAlias(value = "acc_no",isEncrypt = true)
    private String bankNo;

    @Override
    public void doCheck() {
        if(Strings.isBlank(userId)&&Strings.isBlank(bankNo)) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(),"用户ID和银行卡号不能同时为空");
        }
    }
}
