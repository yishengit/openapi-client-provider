package com.acooly.module.openapi.client.provider.yl.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yl.domain.YlApiMsgInfo;
import com.acooly.module.openapi.client.provider.yl.domain.YlRequest;
import com.acooly.module.openapi.client.provider.yl.enums.YlServiceEnum;
import com.acooly.module.openapi.client.provider.yl.message.dto.YlDeductBodyInfo;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@YlApiMsgInfo(service = YlServiceEnum.YL_BATCH_DEDUCT, type = ApiMessageType.Request)
public class YlBatchDeductRequest extends YlRequest {

    /**
     * 业务代码
     */
    private String businessCode;
    /**
     * 商户代码
     */
    private String merchantId;
    /**
     * 预约发送时间 YYYYMMDDHHMMSS 可空 为空表示即收即发
     */
    private String expectSendTime;
    /**
     * 总记录数
     */
    private String totalItem;
    /**
     * 总金额  (整数，单位分)
     */
    private String totalSum;

    List<YlDeductBodyInfo> ylDeductBodyInfos;

}
