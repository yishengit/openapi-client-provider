package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/5/8 17:49
 */
@Getter
@Setter
@XStreamAlias("body")
public class JytRetrievesSmsCodeRequestBody implements Serializable {

    /**
     * 手机号
     */
    @NotBlank
    @Size(max = 13)
    @XStreamAlias("mobile")
    private String mobileNo;

    /**
     * 商户待支付订单号
     */
    @Size(max = 32)
    @XStreamAlias("order_id")
    @NotBlank
    private String orderId;
}
