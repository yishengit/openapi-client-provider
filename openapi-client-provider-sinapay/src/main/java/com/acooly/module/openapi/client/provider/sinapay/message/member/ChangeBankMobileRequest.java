package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/7/10 15:29
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.CHANGE_BANK_MOBILE, type = ApiMessageType.Request)
public class ChangeBankMobileRequest extends SinapayRequest {

    /**
     *修改请求号
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "request_no")
    private String requestNo;

    /**
     *用户标识信息
     * 商户系统用户ID(字母或数字)
     */
    @NotEmpty
    @Size(max = 50)
    @ApiItem(value = "identity_id")
    private String identityId;

    /**
     *用户标识类型
     * ID的类型，目前只包括UID
     */
    @NotEmpty
    @Size(max = 16)
    @ApiItem(value = "identity_type")
    private String identityType = "UID";

    /**
     *卡ID
     * 待修改的银行卡ID
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "card_id")
    private String cardId;

    /**
     *银行预留手机号
     * 密文，使用新浪支付RSA公钥加密。明文长度最大：16。
     */
    @NotEmpty
    @Size(max = 16)
    @ApiItem(value = "phone_no",securet = true)
    private String phoneNo;
}
