package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/6/14 15:11
 */
@Getter
@Setter
@XStreamAlias("body")
public class JytCheckCardApplyResponseBody implements Serializable {

    /**
     * 绑卡编号
     * 当响应码为S0000000时有值。
     * 绑卡短信确认时需要将该字段传回平台
     */
    @XStreamAlias("bind_order_id")
    private String bindOrderNo;
}
