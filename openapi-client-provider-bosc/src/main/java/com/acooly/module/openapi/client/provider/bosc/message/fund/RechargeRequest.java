package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.Validators;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscBankcodeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscExpectPayCompanyEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscRechargeWayEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscBizTypeEnum;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.util.Assert;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiMsgInfo(service = BoscServiceNameEnum.RECHARGE, type = ApiMessageType.Request)
public class RechargeRequest extends BoscRequest {
	/**
	 * 平台用户编号，
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 充值金额
	 */
	@MoneyConstraint(min = 1)
	private Money amount;
	/**
	 * 平台佣金
	 */
	@MoneyConstraint(nullable = true)
	private Money commission;
	/**
	 * 偏好支付公司，见【支付公司】
	 */
	@NotNull
	private BoscExpectPayCompanyEnum expectPayCompany = BoscExpectPayCompanyEnum.BAOFOO;
	/**
	 * 【支付方式】，支持网银（WEB）、快捷支付（SWIFT）
	 */
	@NotNull
	private BoscRechargeWayEnum rechargeWay;
	/**
	 * 【见银行编码】若支付方式为快捷支付，此处必传；若支付方式为网银且传了payType 则此处必传，不传 payTyp 则此处传了也不生效；
	 */
	@NotNull(groups = RechargeSWIFT.class)
	private BoscBankcodeEnum bankcode;
	/**
	 * 【网银类型】网银支付方式下，若此处传值则直接跳转至银行页面，不传则跳转至 支付公司收银台页面；
	 */
	private String payType;
	/**
	 * 【授权交易类型】，若想实现充值+单次授权，则此参数必传
	 */
	private BoscBizTypeEnum authtradeType;
	/**
	 * 授权金额，充值成功后可操作对应金额范围内的授权预处理；若传入了【授权交易 类型】，则此参数必传；
	 */
	@MoneyConstraint(nullable = true)
	private Money authtenderAmount;
	/**
	 * 标的号；若传入了【授权交易类型】，则此参数必传。
	 */
	@Size(max = 50)
	private String projectNo;
	/**
	 * 页面回跳 URL
	 */
	@NotEmpty
	@Size(max = 100)
	private String redirectUrl;
	/**
	 * 超过此时间即页面过期
	 */
	@NotNull
	private Date expired;
	/**
	 * 快捷充值回调模式，如传入 DIRECT_CALLBACK，则订单支付不论成功、失败、 处理中均直接同步、异步通知商户；
	 */
	@Size(max = 50)
	private String callbackMode = "DIRECT_CALLBACK";
	
	public RechargeRequest () {
		setService (BoscServiceNameEnum.RECHARGE.code ());
	}
	
	public RechargeRequest (String platformUserNo, String requestNo, Money amount, Money commission,
	                        BoscRechargeWayEnum rechargeWay, String redirectUrl,Date expired) {
		this();
		this.platformUserNo = platformUserNo;
		this.requestNo = requestNo;
		this.amount = amount;
		this.commission = commission;
		this.rechargeWay = rechargeWay;
		this.redirectUrl = redirectUrl;
		this.expired=expired;
	}
	
	public void doCheck(){
		if(BoscRechargeWayEnum.SWIFT.equals (rechargeWay)){
			Validators.assertJSR303 (this,RechargeSWIFT.class);
		}
		else if(BoscRechargeWayEnum.WEB.equals (rechargeWay)){
			if(StringUtils.isNotBlank (payType)){
				Assert.isTrue (bankcode != null,"网银充值,如果payType不为空,bankCode必须输入");
			}
		}
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public Money getAmount () {
		return amount;
	}
	
	public void setAmount (Money amount) {
		this.amount = amount;
	}
	
	public Money getCommission () {
		return commission;
	}
	
	public void setCommission (Money commission) {
		this.commission = commission;
	}
	
	public BoscExpectPayCompanyEnum getExpectPayCompany () {
		return expectPayCompany;
	}
	
	public void setExpectPayCompany (
			BoscExpectPayCompanyEnum expectPayCompany) {
		this.expectPayCompany = expectPayCompany;
	}
	
	public BoscRechargeWayEnum getRechargeWay () {
		return rechargeWay;
	}
	
	public void setRechargeWay (BoscRechargeWayEnum rechargeWay) {
		this.rechargeWay = rechargeWay;
	}
	
	public BoscBankcodeEnum getBankcode () {
		return bankcode;
	}
	
	public void setBankcode (BoscBankcodeEnum bankcode) {
		this.bankcode = bankcode;
	}
	
	public String getPayType () {
		return payType;
	}
	
	public void setPayType (String payType) {
		this.payType = payType;
	}
	
	public BoscBizTypeEnum getAuthtradeType () {
		return authtradeType;
	}
	
	public void setAuthtradeType (BoscBizTypeEnum authtradeType) {
		this.authtradeType = authtradeType;
	}
	
	public Money getAuthtenderAmount () {
		return authtenderAmount;
	}
	
	public void setAuthtenderAmount (Money authtenderAmount) {
		this.authtenderAmount = authtenderAmount;
	}
	
	public String getProjectNo () {
		return projectNo;
	}
	
	public void setProjectNo (String projectNo) {
		this.projectNo = projectNo;
	}
	
	public String getRedirectUrl () {
		return redirectUrl;
	}
	
	public void setRedirectUrl (String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	
	public Date getExpired () {
		return expired;
	}
	
	public void setExpired (Date expired) {
		this.expired = expired;
	}
	
	public String getCallbackMode () {
		return callbackMode;
	}
	
	public void setCallbackMode (String callbackMode) {
		this.callbackMode = callbackMode;
	}
	
	interface RechargeSWIFT{}
}