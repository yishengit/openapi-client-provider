package com.acooly.module.openapi.client.provider.baofu.message.info;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/2/2 14:24
 */
@Getter
@Setter
@XStreamAlias("trans_reqData")
public class BaoFuWithdrawQueryResponseInfo implements Serializable{

    /**
     * 宝付订单号 宝付订单号返回唯一
     */
    @XStreamAlias("trans_orderid")
    private String transOrderid;

    /**
     * 宝付批次号
     */
    @XStreamAlias("trans_batchid")
    private String transBatchid;

    /**
     * 商户订单号
     */
    @XStreamAlias("trans_no")
    private String transNo;

    /**
     * 转账金额 单位：元
     */
    @XStreamAlias("trans_money")
    private String transMoney;

    /**
     * 收款人姓名
     */
    @XStreamAlias("to_acc_name")
    private String toAccName;

    /**
     * 收款人银行帐号
     */
    @XStreamAlias("to_acc_no")
    private String toAccNo;

    /**
     * 收款人开户行机构名 返回格式：省|市|支行
     */
    @XStreamAlias("to_acc_dept")
    private String toAccDept;

    /**
     * 交易手续费
     */
    @XStreamAlias("trans_fee")
    private String transFee;

    /**
     * 交易手续费 0：转账中；
     *1：转账成功；
     *-1：转账失败；
     *2：转账退款
     */
    @XStreamAlias("state")
    private String state;

    /**
     * 摘要
     */
    @XStreamAlias("trans_summary")
    private String transSummary;


    /**
     * 备注（错误信息）
     */
    @XStreamAlias("trans_remark")
    private String transRemark;

    /**
     * 交易申请时间 格式：yyyyMMddHHmmss
     */
    @XStreamAlias("trans_starttime")
    private String transStarttime;

    /**
     * 交易完成时间 格式：yyyyMMddHHmmss
     */
    @XStreamAlias("trans_endtime")
    private String transEndtime;

    /**
     * 收款方宝付会员号
     */
    @XStreamAlias("to_member_id")
    private String toMemberId;
}
