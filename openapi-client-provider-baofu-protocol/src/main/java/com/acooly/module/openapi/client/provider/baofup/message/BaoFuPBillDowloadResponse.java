package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPResponse;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/30 17:43
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.BILL_DOWNLOAD,type = ApiMessageType.Response)
@XStreamAlias("result")
public class BaoFuPBillDowloadResponse extends BaoFuPResponse {
    /**
     * 返回文件体
     */
    @XStreamAlias("resp_body")
    private String respBody;

    /**
     * 预留域
     */
    @XStreamAlias("reserved")
    private String reserved;

}
