package com.acooly.module.openapi.client.provider.yibao;

import com.acooly.module.openapi.client.api.notify.ApiServiceClientServlet;
import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.yibao.file.YibaoSftpFileHandler;
import com.acooly.module.openapi.client.provider.yibao.notify.YibaoApiServiceClientServlet;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;


@EnableConfigurationProperties({com.acooly.module.openapi.client.provider.yibao.OpenAPIClientYibaoProperties.class})
@ConditionalOnProperty(value = com.acooly.module.openapi.client.provider.yibao.OpenAPIClientYibaoProperties.PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
@ComponentScan("com.acooly.module.openapi.client.file")
public class OpenAPIClientYibaoConfigration {

    @Autowired
    private com.acooly.module.openapi.client.provider.yibao.OpenAPIClientYibaoProperties openAPIClientYibaoProperties;

    @Bean("yibaoHttpTransport")
    public Transport yibaoHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setConnTimeout(String.valueOf(openAPIClientYibaoProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientYibaoProperties.getReadTimeout()));
        httpTransport.setContentType("application/x-www-form-urlencoded");
        httpTransport.setCharset("UTF-8");
        return httpTransport;
    }

    /**
     * 接受异步通知的filter
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean yibaoClientServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        YibaoApiServiceClientServlet apiServiceClientServlet = new YibaoApiServiceClientServlet();
        bean.setServlet(apiServiceClientServlet);
        bean.addInitParameter(ApiServiceClientServlet.NOTIFY_DISPATCHER_BEAN_NAME_KEY, "yibaoNotifyHandlerDispatcher");
        List<String> urlMappings = Lists.newArrayList();       //网关异步通知地址
        urlMappings.add(YibaoConstants.getCanonicalUrl(openAPIClientYibaoProperties.getNotifyUrl(), "/*"));//访问，可以添加多个
        bean.setUrlMappings(urlMappings);
        bean.setLoadOnStartup(3);
        return bean;
    }


    @Bean("yibaoSftpFileHandler")
    public YibaoSftpFileHandler yibaoSftpFileHandler() {
        YibaoSftpFileHandler yibaoSftpFileHandler = new YibaoSftpFileHandler();
        yibaoSftpFileHandler.setHost(openAPIClientYibaoProperties.getCheckfile().getHost());
        yibaoSftpFileHandler.setPort(openAPIClientYibaoProperties.getCheckfile().getPort());
        yibaoSftpFileHandler.setUsername(openAPIClientYibaoProperties.getCheckfile().getUsername());
        yibaoSftpFileHandler.setPassword(openAPIClientYibaoProperties.getCheckfile().getPassword());
        yibaoSftpFileHandler.setServerRoot(openAPIClientYibaoProperties.getCheckfile().getServerRoot());
        yibaoSftpFileHandler.setLocalRoot(openAPIClientYibaoProperties.getCheckfile().getLocalRoot());
        return yibaoSftpFileHandler;
    }


}
