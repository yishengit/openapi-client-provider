package com.acooly.module.openapi.client.provider.hx.message.xStream.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

/**
 * @author fufeng 2018/1/26 15:26.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("Head")
public class RespWithdrawHead {
    /**
     *对应请求报文头的 MsgId，只作为消息传递应答，不作为签名
     *验证的依据
     */
    @XStreamAlias("ReferenceID")
    private String referenceID;
    /**
     *000000#请求响应成功，但不作交易完成的依据
     *999999# //系统异常
     */
    @XStreamAlias("RspCode")
    private String rspCode;
    /**
     *响应消息说明
     */
    @XStreamAlias("RspMsg")
    private String rspMsg;
    /**
     *报文接收时间
     *格式：yyyyMMddHHmmss
     */
    @XStreamAlias("ReqDate")
    private String reqDate;
    /**
     *报文响应时间
     *格式：yyyyMMddHHmmss
     */
    @XStreamAlias("RspDate")
    private String rspDate;
    /**
     *MD5 签名
     *Body 节点不为空 对<body>……</body>节点字符串+商户号+
     *商户 MD5 证书进行签名（包括 body 标签）
     *body 节点为空时 使用商户号+商户证书的进行签名
     *注意：+号在此处代表字符串连接
     */
    @XStreamAlias("Signature")
    private String signature;

}
