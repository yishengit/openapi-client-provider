package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayRequest;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike 2018/5/17 18:58
 */
@Getter
@Setter
@ShengpayApiMsg(service = ShengpayServiceNameEnum.PAYMENT_CONFIRM,type = ApiMessageType.Request)
public class ShengpayPaymentConfirmRequest extends ShengpayRequest {

    /**
     * 支付token
     */
    @NotBlank
    private String sessionToken;

    /**
     * 短信验证码
     */
    @NotBlank
    private String validateCode;

    /**
     * 用户IP （用户支付时的IP）
     */
    @NotBlank
    private String userIp;
}
