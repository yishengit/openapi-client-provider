package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscCheckFileTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.ConfirmCheckInfo;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * 对账确认
 * Created by liubin@prosysoft.com on 2017/10/10.
 */
@ApiMsgInfo(service = BoscServiceNameEnum.CONFIRM_CHECKFILE
		, type = ApiMessageType.Request)
public class ConfirmCheckFileRequest extends BoscRequest {
	
	@NotBlank(message = "请求流水号不能为空")
	@Size(max = 50)
	private String requestNo;
	
	@NotBlank(message = "对账文件日期不能为空")
	private String fileDate;
	
	@NotEmpty(message = "确认文件类型不能为空")
	private List<ConfirmCheckInfo> detail = new ArrayList<> ();
	
	
	public ConfirmCheckFileRequest () {
		setService (BoscServiceNameEnum.CONFIRM_CHECKFILE
				            .code ());
		detail.add (new ConfirmCheckInfo (BoscCheckFileTypeEnum.RECHARGE));
		detail.add (new ConfirmCheckInfo (BoscCheckFileTypeEnum.COMMISSION));
		detail.add (new ConfirmCheckInfo (BoscCheckFileTypeEnum.BACKROLL_RECHARGE));
		detail.add (new ConfirmCheckInfo (BoscCheckFileTypeEnum.TRANSACTION));
		detail.add (new ConfirmCheckInfo (BoscCheckFileTypeEnum.WITHDRAW));
	}
	
	public ConfirmCheckFileRequest (String requestNo, String fileDate) {
		this();
		this.requestNo = requestNo;
		this.fileDate = fileDate;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public List<ConfirmCheckInfo> getDetail () {
		return detail;
	}
	
	public void setDetail (
			List<ConfirmCheckInfo> detail) {
		this.detail = detail;
	}
	
	public String getFileDate () {
		return fileDate;
	}
	
	public void setFileDate (String fileDate) {
		this.fileDate = fileDate;
	}
}
