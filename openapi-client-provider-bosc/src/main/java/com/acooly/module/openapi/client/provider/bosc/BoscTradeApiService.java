package com.acooly.module.openapi.client.provider.bosc;

import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscBizTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.QueryTransactionDebentureSaleResponse;
import com.acooly.module.openapi.client.provider.bosc.message.fund.QueryTransactionPretransactionResponse;
import com.acooly.module.openapi.client.provider.bosc.message.fund.QueryTransactionRequest;
import com.acooly.module.openapi.client.provider.bosc.message.trade.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.Resource;

/**
 * 交易相关的api
 * Created by liubin@prosysoft.com on 2017/9/27.
 */
@Component
public class BoscTradeApiService {
	
	@Resource(name = "boscApiServiceClient")
	private BoscApiServiceClient boscApiServiceClient;
	
	/**
	 * 创建标的
	 * @param request
	 * @return
	 */
	public EstablishProjectResponse establishProject (EstablishProjectRequest request) {
		return (EstablishProjectResponse) boscApiServiceClient.execute (request);
	}
	
	/**
	 * 修改标的状态
	 * @param request
	 * @return
	 */
	public ModifyProjectResponse modifyProjectRequest(ModifyProjectRequest request){
		return (ModifyProjectResponse) boscApiServiceClient.execute (request);
	}
	
	/**
	 * 查询标的信息
	 * @param request
	 * @return
	 */
	public QueryProjectInformationResponse queryProjectInformation (QueryProjectInformationRequest request) {
		return (QueryProjectInformationResponse) boscApiServiceClient.execute (request);
	}
	
	/**
	 * 委托支付授权
	 * @param request
	 * @return
	 */
	public AuthorizationEntrustPayResponse authorizationEntrustPay (AuthorizationEntrustPayRequest request) {
		return (AuthorizationEntrustPayResponse) boscApiServiceClient.execute (request);
	}
	
	/**
	 * 委托支付授权查询
	 * @param recordRequest
	 * @return
	 */
	public QueryAuthorizationEntrustPayRecordResponse queryAuthorizationEntrustPayRecord (
			QueryAuthorizationEntrustPayRecordRequest recordRequest) {
		return (QueryAuthorizationEntrustPayRecordResponse) boscApiServiceClient.execute (recordRequest);
	}
	
	/**
	 * 债权转让
	 * @param request
	 * @return
	 */
	public DebentureSaleResponse debentureSale(DebentureSaleRequest request){
		return (DebentureSaleResponse) boscApiServiceClient.execute (request);
	}
	
	/**
	 * 取消债转
	 * @param request
	 * @return
	 */
	public CancelDebentureSaleResponse cancelDebentureSale(CancelDebentureSaleRequest request){
		return (CancelDebentureSaleResponse) boscApiServiceClient.execute (request);
	}
	
	/**
	 * 用户授权预处理
	 *
	 * @param request
	 *
	 * @return
	 */
	public UserAutoPreTransactionResponse userAutoPreTransaction (UserAutoPreTransactionRequest request) {
		Assert.notNull (request.getBizType ());
		
		validateUserAutoPreTransBizType (request);
		if (BoscBizTypeEnum.CREDIT_ASSIGNMENT.equals (request.getBizType ())) {
			Validators.assertJSR303 (request, UserAutoPreTransactionRequest.CreditAssignGroup.class);
		}
		
		return (UserAutoPreTransactionResponse) boscApiServiceClient.execute (request);
	}
	
	
	private void validateUserAutoPreTransBizType (UserAutoPreTransactionRequest request) {
		
		
		if (!(BoscBizTypeEnum.TENDER.equals (request.getBizType ()) ||
				BoscBizTypeEnum.CREDIT_ASSIGNMENT.equals (request.getBizType ()) ||
				BoscBizTypeEnum.REPAYMENT.equals (request.getBizType ()) ||
				BoscBizTypeEnum.COMPENSATORY.equals (request.getBizType ()))) {
			
			throw new IllegalArgumentException ("授权预处理只支持:买标、债转、还款、代偿.");
		}
	}
	
	
	/**
	 * 用户授权预处理取消
	 * @param request
	 * @return
	 */
	public CancelPreTransactionResponse cancelPreTransaction(CancelPreTransactionRequest request){
		return (CancelPreTransactionResponse) boscApiServiceClient.execute (request);
	}
	
	/**
	 * 查询债转交易明细
	 * @param request
	 * @return
	 */
	public QueryTransactionDebentureSaleResponse queryTransactionDebentureSale(QueryTransactionRequest request){
		return (QueryTransactionDebentureSaleResponse) boscApiServiceClient.executeTransactionQuery (request);
	}
	
	/**
	 * 查询预处理交易明细
	 * @param request
	 * @return
	 */
	public QueryTransactionPretransactionResponse queryTransactionPretransaction(QueryTransactionRequest request){
		return (QueryTransactionPretransactionResponse) boscApiServiceClient.executeTransactionQuery (request);
		
	}
	
	
}
