/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.yinsheng.domain;

import com.acooly.core.utils.Dates;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author zhangpu
 */
@Getter
@Setter
public class YinShengRequest extends YinShengApiMessage {

    /**
     * 交易起始时间
     * 发送请求的时间 ,格式"yyyy-MM-dd HH:mm:ss
     */
    private String timestamp = Dates.format(new Date(), "yyyy-MM-dd HH:mm:ss");

    /**
     * 服务版本
     */
    private String version = "3.0";

    /**
     * 编码
     */
    private String charset = "UTF-8";
}
