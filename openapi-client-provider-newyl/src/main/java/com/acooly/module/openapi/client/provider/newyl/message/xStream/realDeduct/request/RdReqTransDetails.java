package com.acooly.module.openapi.client.provider.newyl.message.xStream.realDeduct.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.List;

/**
 * @author fufeng 2018/1/26 15:29.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("TRANS_DETAILS")
public class RdReqTransDetails {
    @XStreamImplicit(itemFieldName="TRANS_DETAIL")
    private List<RdReqTransDetail> transDetail;

}
