package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscNotify;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscBankcodeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscExpectPayCompanyEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscRechargeStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscRechargeWayEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiMsgInfo(service = BoscServiceNameEnum.RECHARGE, type = ApiMessageType.Notify)
public class RechargeNotify extends BoscNotify {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 充值状态；SUCCESS 支付成功、FAIL 支付失败、PENDDING 支付中（充值结果 以此状态为准）
	 */
	@NotNull
	private BoscRechargeStatusEnum rechargeStatus;
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 充值金额
	 */
	@MoneyConstraint(min = 1)
	private Money amount;
	/**
	 * 平台佣金
	 */
	@MoneyConstraint(nullable = true)
	private Money commission;
	/**
	 * 见【支付公司】
	 */
	@NotNull
	private BoscExpectPayCompanyEnum payCompany;
	/**
	 * 见【支付方式】
	 */
	@NotNull
	private BoscRechargeWayEnum rechargeWay;
	/**
	 * 见【银行编码】（只有快捷会返回）
	 */
	private BoscBankcodeEnum bankcode;
	/**
	 * 本次充值手机号，网银为空
	 */
	@Size(max = 50)
	private String payMobile;
	/**
	 * 交易完成时间
	 */
	private Date transactionTime;
	/**
	 * 见【支付通道错误码】（若快捷充值回调方式传入 DIRECT_CALLBACK，则返回 此参数）技术支持单独提供
	 */
	@Size(max = 50)
	private String channelErrorCode;
	/**
	 * 见【支付通道返回错误消息】（若快捷充值回调方式传入 DIRECT_CALLBACK， 则返回此参数）技术支持单独提供
	 */
	@Size(max = 200)
	private String channelErrorMessage;
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public BoscRechargeStatusEnum getRechargeStatus () {
		return rechargeStatus;
	}
	
	public void setRechargeStatus (
			BoscRechargeStatusEnum rechargeStatus) {
		this.rechargeStatus = rechargeStatus;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public Money getAmount () {
		return amount;
	}
	
	public void setAmount (Money amount) {
		this.amount = amount;
	}
	
	public Money getCommission () {
		return commission;
	}
	
	public void setCommission (Money commission) {
		this.commission = commission;
	}
	
	public BoscExpectPayCompanyEnum getPayCompany () {
		return payCompany;
	}
	
	public void setPayCompany (BoscExpectPayCompanyEnum payCompany) {
		this.payCompany = payCompany;
	}
	
	public BoscRechargeWayEnum getRechargeWay () {
		return rechargeWay;
	}
	
	public void setRechargeWay (BoscRechargeWayEnum rechargeWay) {
		this.rechargeWay = rechargeWay;
	}
	
	public BoscBankcodeEnum getBankcode () {
		return bankcode;
	}
	
	public void setBankcode (BoscBankcodeEnum bankcode) {
		this.bankcode = bankcode;
	}
	
	public String getPayMobile () {
		return payMobile;
	}
	
	public void setPayMobile (String payMobile) {
		this.payMobile = payMobile;
	}
	
	public Date getTransactionTime () {
		return transactionTime;
	}
	
	public void setTransactionTime (Date transactionTime) {
		this.transactionTime = transactionTime;
	}
	
	public String getChannelErrorCode () {
		return channelErrorCode;
	}
	
	public void setChannelErrorCode (String channelErrorCode) {
		this.channelErrorCode = channelErrorCode;
	}
	
	public String getChannelErrorMessage () {
		return channelErrorMessage;
	}
	
	public void setChannelErrorMessage (String channelErrorMessage) {
		this.channelErrorMessage = channelErrorMessage;
	}
}