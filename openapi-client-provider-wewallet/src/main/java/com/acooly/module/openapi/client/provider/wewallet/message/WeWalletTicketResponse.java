package com.acooly.module.openapi.client.provider.wewallet.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletApiMsgInfo;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletResponse;
import com.acooly.module.openapi.client.provider.wewallet.enums.WeWalletServiceEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@WeWalletApiMsgInfo(service = WeWalletServiceEnum.WEWALLET_TICKET, type = ApiMessageType.Response)
public class WeWalletTicketResponse extends WeWalletResponse {

    private String bizSeqNo;

    private String transactionTime;

    private String success;

    private String tickets;

    private String expireIn;

    private String expireTime;

}
