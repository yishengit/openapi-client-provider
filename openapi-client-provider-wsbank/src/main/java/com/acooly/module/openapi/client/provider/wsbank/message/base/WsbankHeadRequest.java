package com.acooly.module.openapi.client.provider.wsbank.message.base;

import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Ids;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhike 2018/5/22 15:29
 */
@Getter
@Setter
@XStreamAlias("head")
public class WsbankHeadRequest implements Serializable {

	private static final long serialVersionUID = 3245033037667808003L;

	/**
     * 版本号
     * 匹配接口文档版本。联调环境统一送1.0.0
     */
    @XStreamAlias("Version")
    private String version = "1.0.0";

    /**
     * 应用ID
     * 由浙江网商银行统一分配,用于识别合作伙伴应用系统，即对端系统编号。联调前线下提供。注意此字段此处大小写要求。
     */
    @XStreamAlias("Appid")
    @NotBlank
    private String appid;

    /**
     * 接口代码
     * 接口定义中的报文编号，明确接口功能，关联接口文档
     */
    @XStreamAlias("Function")
    @NotBlank
    private String function;

    /** 报文发起时间
     *  格式：yyyyMMddHHmmss，请求发起时间
     */
    @XStreamAlias("ReqTime")
    @NotBlank
    private String reqTime = Dates.format(new Date(),"yyyyMMddHHmmss");

    /**
     *报文发起时区
     * 请求发起系统所在时区
     */
    @XStreamAlias("ReqTimeZone")
    private String reqTimeZone = "UTC+8";

    /** 请求报文ID 唯一定位一次报文请求，由发起方生成，应答方原样返回，uuid生成，全局唯一 */
    @XStreamAlias("ReqMsgId")
    @NotBlank
    private String reqMsgId = Ids.oid();

    /**
     * 报文字符编码
     */
    @XStreamAlias("InputCharset")
    @NotBlank
    private String inputCharset = "UTF-8";

    /**
     * 保留字段
     * 使用KV方式表达
     */
    @XStreamAlias("Reserve")
    private String reserve;

    /**
     * 签名方式
     */
    @XStreamAlias("SignType")
    private String signType = "RSA";
}
