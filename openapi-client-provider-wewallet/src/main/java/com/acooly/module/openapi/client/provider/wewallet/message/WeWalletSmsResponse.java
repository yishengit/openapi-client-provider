package com.acooly.module.openapi.client.provider.wewallet.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletApiMsgInfo;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletResponse;
import com.acooly.module.openapi.client.provider.wewallet.enums.WeWalletServiceEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@WeWalletApiMsgInfo(service = WeWalletServiceEnum.WEBANK_SMS, type = ApiMessageType.Response)
public class WeWalletSmsResponse extends WeWalletResponse {

    /**
     * 商户号
     */
    String merId;

    /**
     * 订单号
     */
    String orderId;

    /**
     * 交易类型
     */
    String bizType;

}
