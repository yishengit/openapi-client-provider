/**
 * coding by zhangpu
 */
package com.acooly.module.openapi.client.provider.jyt.notify;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.ApiServiceClient;
import com.acooly.module.openapi.client.api.notify.AbstractSpringNotifyHandlerDispatcher;
import com.acooly.module.openapi.client.provider.jyt.JytApiServiceClient;
import com.acooly.module.openapi.client.provider.jyt.OpenAPIClientJytProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 富友 支付网关异步通知分发器
 *
 * @author liuyuxiang
 * @date 2016年5月12日
 */
@Component
public class JytNotifyHandlerDispatcher extends AbstractSpringNotifyHandlerDispatcher {

    @Autowired
    private OpenAPIClientJytProperties openAPIClientJytProperties;

    @Resource(name = "jytApiServiceClient")
    private JytApiServiceClient apiServiceClient;

    @Override
    protected String getServiceKey(String notifyUrl, Map<String, String> notifyData) {
        return Strings.substringAfterLast(notifyUrl, openAPIClientJytProperties.getNotifyUrlPrefix());
    }

    @Override
    protected ApiServiceClient getApiServiceClient() {
        return apiServiceClient;
    }
}
