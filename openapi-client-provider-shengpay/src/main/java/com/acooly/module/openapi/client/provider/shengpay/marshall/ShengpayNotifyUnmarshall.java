/**
 * create by zhike
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.shengpay.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.provider.shengpay.ShengpayConstants;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayNotify;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhike
 */
@Slf4j
@Component
public class ShengpayNotifyUnmarshall extends ShengpayAbstractMarshall implements ApiUnmarshal<ShengpayNotify, Map<String, String>> {
    
    @Override
    public ShengpayNotify unmarshal(Map<String, String> message, String serviceKey) {
        return (ShengpayNotify) doUnMarshall(message, serviceKey, true);
    }
}
