package com.acooly.module.openapi.client.provider.jyt.message;

import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.jyt.domain.JytApiMsgInfo;
import com.acooly.module.openapi.client.provider.jyt.domain.JytRequest;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytDeductPayRequestBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/8 15:45
 */
@Getter
@Setter
@JytApiMsgInfo(service = JytServiceEnum.DEDUCT_PAY,type = ApiMessageType.Request)
@XStreamAlias("message")
public class JytDeductPayRequest extends JytRequest {

    /**
     * 请求报文体
     */
    @XStreamAlias("body")
    private JytDeductPayRequestBody deductPayRequestBody;

    @Override
    public void doCheck() {
        Validators.assertJSR303(getHeaderRequest());
        Validators.assertJSR303(getDeductPayRequestBody());
    }
}
