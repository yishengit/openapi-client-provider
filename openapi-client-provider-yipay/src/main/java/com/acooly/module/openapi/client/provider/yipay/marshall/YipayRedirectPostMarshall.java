/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 16:11 创建
 */
package com.acooly.module.openapi.client.provider.yipay.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.yipay.YipayConstants;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 跳转请求报文组装
 *
 * @author zhangpu 2017-09-24 16:11
 */
@Slf4j
@Component
public class YipayRedirectPostMarshall extends YipayAbstractMarshall implements ApiMarshal<PostRedirect, YipayRequest> {

    @Override
    public PostRedirect marshal(YipayRequest source) {
        PostRedirect postRedirect = new PostRedirect();
        postRedirect.setRedirectUrl(YipayConstants.getCanonicalUrl(getProperties().getGatewayUrl(),
                YipayConstants.getServiceName(source)));
        postRedirect.addData(YipayConstants.REQUEST_PARAM_NAME, doMarshall(source));
        log.info("跳转报文: {}", postRedirect);
        return postRedirect;
    }


}