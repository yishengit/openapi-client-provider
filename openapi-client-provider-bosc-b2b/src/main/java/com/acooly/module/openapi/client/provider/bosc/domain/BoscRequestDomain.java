package com.acooly.module.openapi.client.provider.bosc.domain;

import com.acooly.module.openapi.client.provider.bosc.annotation.BoscAlias;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoscRequestDomain extends BoscBaseDomain {

	/**
	 * 请求体参数-渠道类型
	 */
	@BoscAlias(isSign = false)
	private String channelUrl;

	/**
	 * 渠道ID
	 */
//	@BoscAlias(value = "channelId")
	@BoscAlias(isSign = false)
	private String channels;

}
