//package com.acooly.module.openapi.client.provider.baofup.partner.impl;
//
//import com.acooly.core.utils.security.RSA;
//import com.acooly.module.openapi.client.provider.baofup.BaoFuPConstants;
//import com.acooly.module.openapi.client.provider.baofup.OpenAPIClientBaoFuPProperties;
//import com.acooly.module.safety.key.AbstractKeyLoadManager;
//import com.acooly.module.safety.key.KeyStoreLoader;
//import com.acooly.module.safety.support.KeyStoreInfo;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//@Component
//public class BaoFuPDefaultLoadKeyStoreService extends AbstractKeyLoadManager<KeyStoreInfo> implements KeyStoreLoader {
//
//    @Autowired
//    private OpenAPIClientBaoFuPProperties openAPIClientBaoFuPProperties;
//
//    @Override
//    public KeyStoreInfo doLoad(String principal) {
//        KeyStoreInfo keyStoreInfo = new KeyStoreInfo();
//        keyStoreInfo.setCertificateUri(openAPIClientBaoFuProperties.getPublicKeyPath());
//        keyStoreInfo.setKeyStoreUri(openAPIClientBaoFuProperties.getPrivateKeyPath());
//        keyStoreInfo.setKeyStorePassword(openAPIClientBaoFuProperties.getPrivateKeyPassword());
//        keyStoreInfo.setPlainEncode("UTF-8");
//        keyStoreInfo.setKeyStoreType(RSA.KEY_STORE_PKCS12);
//        keyStoreInfo.loadKeys();
//        return keyStoreInfo;
//    }
//
//    @Override
//    public String getProvider() {
//        return BaoFuPConstants.PROVIDER_NAME;
//    }
//}
