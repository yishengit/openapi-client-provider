package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;

@ApiMsgInfo(service = BoscServiceNameEnum.ESTABLISH_PROJECT, type = ApiMessageType.Response)
public class EstablishProjectResponse extends BoscResponse {

}