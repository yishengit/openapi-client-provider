package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscEntrustedTypeEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.AUTHORIZATION_ENTRUST_PAY, type = ApiMessageType.Request)
public class AuthorizationEntrustPayRequest extends BoscRequest {
	/**
	 * 借款方平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String borrowPlatformUserNo;
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 标的号
	 */
	@NotEmpty
	private String projectNo;
	/**
	 * 鉴权验证类型，默认填 LIMIT
	 */
	@NotNull
	private String checkType = "LIMIT";
	/**
	 * 受托方类型，枚举值 PERSONAL（个人），ENTERPRISE（企业）
	 */
	@NotNull
	private BoscEntrustedTypeEnum entrustedType;
	/**
	 * 50
	 */
	@NotEmpty
	private String entrustedPlatformUserNo;
	
	public AuthorizationEntrustPayRequest () {
		setService (BoscServiceNameEnum.AUTHORIZATION_ENTRUST_PAY.code ());
	}
	
	public AuthorizationEntrustPayRequest (String borrowPlatformUserNo, String requestNo, String projectNo,
	                                       BoscEntrustedTypeEnum entrustedType, String entrustedPlatformUserNo) {
		this();
		this.borrowPlatformUserNo = borrowPlatformUserNo;
		this.requestNo = requestNo;
		this.projectNo = projectNo;
		this.entrustedType = entrustedType;
		this.entrustedPlatformUserNo = entrustedPlatformUserNo;
	}
	
	public String getBorrowPlatformUserNo () {
		return borrowPlatformUserNo;
	}
	
	public void setBorrowPlatformUserNo (String borrowPlatformUserNo) {
		this.borrowPlatformUserNo = borrowPlatformUserNo;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getProjectNo () {
		return projectNo;
	}
	
	public void setProjectNo (String projectNo) {
		this.projectNo = projectNo;
	}
	
	public String getCheckType () {
		return checkType;
	}
	
	public void setCheckType (String checkType) {
		this.checkType = checkType;
	}
	
	public BoscEntrustedTypeEnum getEntrustedType () {
		return entrustedType;
	}
	
	public void setEntrustedType (BoscEntrustedTypeEnum entrustedType) {
		this.entrustedType = entrustedType;
	}
	
	public String getEntrustedPlatformUserNo () {
		return entrustedPlatformUserNo;
	}
	
	public void setEntrustedPlatformUserNo (String entrustedPlatformUserNo) {
		this.entrustedPlatformUserNo = entrustedPlatformUserNo;
	}
}