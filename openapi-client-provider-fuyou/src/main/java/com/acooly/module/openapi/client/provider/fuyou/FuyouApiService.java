/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.fuyou;

import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouNotify;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.acooly.module.openapi.client.provider.fuyou.message.*;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouAgreementPayRequest;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouAgreementPayResponse;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouAgrpayGetCodeOrderRequest;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouAgrpayGetCodeOrderResponse;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouAgrpayRetCodeOrderRequest;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouAgrpayRetCodeOrderResponse;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouFirstAgreementPayRequest;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouFirstAgreementPayResponse;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouFirstAgrpayRetCodeOrderRequest;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouFirstAgrpayRetCodeOrderResponse;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouFirstQuickCreateOrderRequest;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouFirstQuickCreateOrderResponse;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouMerSupportCardBinQueryRequest;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouMerSupportCardBinQueryResponse;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouQuotaQueryRequest;
import com.acooly.module.openapi.client.provider.fuyou.message.FuyouQuotaQueryResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zhangpu
 */
@Service
public class FuyouApiService {

    @Resource(name = "fuyouApiServiceClient")
    private FuyouApiServiceClient apiServiceClient;

    @Autowired
    private OpenAPIClientFuyouProperties fuyouOpenAPIClientFuiouProperties;


    /**
     * 3.1测试首次协议下单，验证码获取接口
     * @param request
     * @return
     */
    public FuyouFirstQuickCreateOrderResponse quickCreateOrder(FuyouFirstQuickCreateOrderRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.FIRST_QUICKPAY_CREATE_ORDER.getKey()+".pay");
        request.setService(FuyouServiceEnum.FIRST_QUICKPAY_CREATE_ORDER.getCode());
        request.setVersion("3.0");
        request.setNotifyUrl(fuyouOpenAPIClientFuiouProperties.getDomain()+"/gateway/notify/fuyouNotify/" + FuyouServiceEnum.FIRST_AGREEMENT_PAY.getCode());
        return (FuyouFirstQuickCreateOrderResponse)apiServiceClient.execute(request);
    }

    /**
     * 3.2测试首次协议支付接口
     * @param request
     * @return
     */
    public FuyouFirstAgreementPayResponse firstAgreementPay(FuyouFirstAgreementPayRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.FIRST_AGREEMENT_PAY.getKey()+".pay");
        request.setService(FuyouServiceEnum.FIRST_AGREEMENT_PAY.getCode());
        request.setVersion("3.0");
        return (FuyouFirstAgreementPayResponse)apiServiceClient.execute(request);
    }

    /**
     * 3.3商户首次协议下单重发验证码
     * @param request
     * @return
     */
    public FuyouFirstAgrpayRetCodeOrderResponse firstAgrpayRetCodeOrder(FuyouFirstAgrpayRetCodeOrderRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.FIRST_AGRPAY_RETCODE_ORDER.getKey()+".pay");
        request.setService(FuyouServiceEnum.FIRST_AGRPAY_RETCODE_ORDER.getCode());
        request.setNotifyUrl(fuyouOpenAPIClientFuiouProperties.getDomain()+"/gateway/notify/fuyouNotify/" + FuyouServiceEnum.FIRST_AGREEMENT_PAY.getCode());
        request.setVersion("3.0");
        return (FuyouFirstAgrpayRetCodeOrderResponse)apiServiceClient.execute(request);
    }

    /**
     * 3.4协议下单验证码获取
     * @param request
     * @return
     */
    public FuyouAgrpayGetCodeOrderResponse agrpayGetCodeOrder(FuyouAgrpayGetCodeOrderRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.AGRPAY_GETCODE_ORDER.getKey()+".pay");
        request.setService(FuyouServiceEnum.AGRPAY_GETCODE_ORDER.getCode());
        request.setNotifyUrl(fuyouOpenAPIClientFuiouProperties.getDomain()+"/gateway/notify/fuyouNotify/" + FuyouServiceEnum.AGREEMENT_PAY.getCode());
        request.setVersion("3.0");
        return (FuyouAgrpayGetCodeOrderResponse)apiServiceClient.execute(request);
    }


    /**
     * 3.5协议下单重发验证码
     * @param request
     * @return
     */
    public FuyouAgrpayRetCodeOrderResponse agrpayRetCodeOrder(FuyouAgrpayRetCodeOrderRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.AGRPAY_RETCODE_ORDER.getKey()+".pay");
        request.setService(FuyouServiceEnum.AGRPAY_RETCODE_ORDER.getCode());
        request.setVersion("3.0");
        request.setNotifyUrl(fuyouOpenAPIClientFuiouProperties.getDomain()+"/gateway/notify/fuyouNotify/" + FuyouServiceEnum.AGREEMENT_PAY.getCode());
        return (FuyouAgrpayRetCodeOrderResponse)apiServiceClient.execute(request);
    }

    /**
     * 3.6协议支付
     * @param request
     * @return
     */
    public FuyouAgreementPayResponse agreementPay(FuyouAgreementPayRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.AGREEMENT_PAY.getKey()+".pay");
        request.setService(FuyouServiceEnum.AGREEMENT_PAY.getCode());
        request.setVersion("3.0");
        return (FuyouAgreementPayResponse)apiServiceClient.execute(request);
    }

    /**
     * 3.7协议解绑
     * @param request
     * @return
     */
    public FuyouAgreementUnbundlingResponse agreementUnbundling(FuyouAgreementUnbundlingRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.AGREEMENT_UNBUNDLING.getKey()+".pay");
        request.setService(FuyouServiceEnum.AGREEMENT_UNBUNDLING.getCode());
        request.setVersion("3.0");
        return (FuyouAgreementUnbundlingResponse)apiServiceClient.execute(request);
    }


    /**
     * 3.8协议卡查询
     * @param request
     * @return
     */
    public FuyouAgreementCardQueryResponse agreementCardQuery(FuyouAgreementCardQueryRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.AGREEMENT_CARD_QUERY.getKey()+".pay");
        request.setService(FuyouServiceEnum.AGREEMENT_CARD_QUERY.getCode());
        request.setVersion("3.0");
        return (FuyouAgreementCardQueryResponse)apiServiceClient.execute(request);
    }

    /**
     * 3.9订单结果查询（富友订单号）
     * @param request
     * @return
     */
    public FuyouOrderResultQueryResponse fuyouOrderResultQuery(FuyouOrderResultQueryRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.FUYOU_ORDER_RESULT_QUERY.getKey()+".pay");
        request.setService(FuyouServiceEnum.FUYOU_ORDER_RESULT_QUERY.getCode());
        request.setVersion("3.0");
        return (FuyouOrderResultQueryResponse)apiServiceClient.execute(request);
    }

    /**
     * 3.10  订单结果查询接口（商户订单号）
     * @param request
     * @return
     */
    public FuyouMerOrderResultQueryResponse merOrderResultQuery(FuyouMerOrderResultQueryRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.MER_ORDER_RESULT_QUERY.getKey()+".pay");
        request.setService(FuyouServiceEnum.MER_ORDER_RESULT_QUERY.getCode());
        request.setVersion("1.0");
        return (FuyouMerOrderResultQueryResponse)apiServiceClient.execute(request);
    }

    /**
     * 3.11 未支付订单关闭
     * @param request
     * @return
     */
    public FuyouUnpaidOrderClosedResponse unpaidOrderClosed(FuyouUnpaidOrderClosedRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.UNPAID_ORDER_CLOSED.getKey()+".pay");
        request.setService(FuyouServiceEnum.UNPAID_ORDER_CLOSED.getCode());
        request.setVersion("3.0");
        return (FuyouUnpaidOrderClosedResponse)apiServiceClient.execute(request);
    }

    /**
     * 3.12商户支持卡Bin查询接口
     * @param request
     * @return
     */
    public FuyouMerSupportCardBinQueryResponse merSupportCardBinQuery(FuyouMerSupportCardBinQueryRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.MERSUPPORT_CARDBIN_QUERY.getKey()+".pay");
        request.setService(FuyouServiceEnum.MERSUPPORT_CARDBIN_QUERY.getCode());
        request.setVersion("3.0");
        return (FuyouMerSupportCardBinQueryResponse)apiServiceClient.execute(request);
    }

    /**
     * 3.13限额查询
     * @param request
     * @return
     */
    public FuyouQuotaQueryResponse quotaQuery(FuyouQuotaQueryRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.QUOTA_QUERY.getKey()+".pay");
        request.setService(FuyouServiceEnum.QUOTA_QUERY.getCode());
        request.setVersion("3.0");
        return (FuyouQuotaQueryResponse)apiServiceClient.execute(request);
    }


    /**
     * 网关跳转充值
     *
     * @param request
     * @return
     */
    public String netBankDeposit(FuyouNetBankRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getNetbankPayUrl()+FuyouServiceEnum.FUYOU_NETBANK.getKey()+".do");
        request.setService(FuyouServiceEnum.FUYOU_NETBANK.getCode());
        request.setBackNotifyUrl(fuyouOpenAPIClientFuiouProperties.getDomain()+"/gateway/notify/fuyouNotify/" + FuyouServiceEnum.FUYOU_NETBANK.getCode());
        return apiServiceClient.redirectGet(request);
    }

    /**
     * 网关充值订单查询
     *
     * @param request
     * @return
     */
    public FuyouNetBankSynchroQueryResponse netBankDepositQuery(FuyouNetBankSynchroQueryRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getNetbankPayUrl()+FuyouServiceEnum.FUYOU_NETBANK_SYNCHRO_QUERY.getKey()+".do");
        request.setService(FuyouServiceEnum.FUYOU_NETBANK_SYNCHRO_QUERY.getCode());
        return (FuyouNetBankSynchroQueryResponse)apiServiceClient.execute(request);
    }


    /**
     * 提现接口
     *
     * @param request
     * @return
     */
    public FuyouWithdrawResponse withdraw(FuyouWithdrawRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getWithdrawUrl());
        request.setService(FuyouServiceEnum.FUYOU_WITHDRAW.getCode());
        request.setReqtype("payforreq");
        return (FuyouWithdrawResponse)apiServiceClient.execute(request);
    }


    /**
     * 提现查询接口
     *
     * @param request
     * @return
     */
    public FuyouWithdrawQueryResponse withdrawQuery(FuyouWithdrawQueryRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getWithdrawUrl());
        request.setService(FuyouServiceEnum.FUYOU_WITHDRAW_QUERY.getCode());
        request.setReqtype("qrytransreq");
        return (FuyouWithdrawQueryResponse)apiServiceClient.execute(request);
    }

    /**
     * 协议卡绑定发送短信验证码1.3
     * @param request
     * @return
     */
    public FuyouBindMsgResponse bindMsg(FuyouBindMsgRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.FUYOU_BIND_MSG.getKey()+".pay");
        request.setService(FuyouServiceEnum.FUYOU_BIND_MSG.getCode());
        request.setVersion("1.0");
        return (FuyouBindMsgResponse)apiServiceClient.execute(request);
    }

    /**
     * 协议卡绑定1.3
     * @param request
     * @return
     */
    public FuyouBindCommitResponse bindCommit(FuyouBindCommitRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.FUYOU_BIND_COMMIT.getKey()+".pay");
        request.setService(FuyouServiceEnum.FUYOU_BIND_COMMIT.getCode());
        request.setVersion("1.0");
        return (FuyouBindCommitResponse)apiServiceClient.execute(request);
    }

    /**
     * 协议解绑1.3
     * @param request
     * @return
     */
    public FuyouUnBindResponse unBind(FuyouUnBindRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.FUYOU_UN_BIND.getKey()+".pay");
        request.setService(FuyouServiceEnum.FUYOU_UN_BIND.getCode());
        request.setVersion("1.0");
        return (FuyouUnBindResponse)apiServiceClient.execute(request);
    }

    /**
     * 协议卡查询1.3
     * @param request
     * @return
     */
    public FuyouBindQueryResponse bindQuery(FuyouBindQueryRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.FUYOU_BIND_QUERY.getKey()+".pay");
        request.setService(FuyouServiceEnum.FUYOU_BIND_QUERY.getCode());
        request.setVersion("1.0");
        return (FuyouBindQueryResponse)apiServiceClient.execute(request);
    }

    /**
     * 协议卡支付1.3
     * @param request
     * @return
     */
    public FuyouOrderPayResponse orderPay(FuyouOrderPayRequest request) {
        request.setGatewayUrl(fuyouOpenAPIClientFuiouProperties.getQuickPayUrl()+FuyouServiceEnum.FUYOU_ORDER_PAY.getKey()+".pay");
        request.setService(FuyouServiceEnum.FUYOU_ORDER_PAY.getCode());
        request.setVersion("1.0");
        request.setNotifyUrl(fuyouOpenAPIClientFuiouProperties.getDomain()+"/gateway/notify/fuyouNotify/" + FuyouServiceEnum.FUYOU_ORDER_PAY.getCode());
        return (FuyouOrderPayResponse)apiServiceClient.execute(request);
    }
    /**
     * 解析同步、异步通知
     *
     * @param request
     * @param serviceKey
     * @return
     */
    public FuyouNotify notice(HttpServletRequest request, String serviceKey) {
        return  apiServiceClient.notice(request,serviceKey);
    }
}
